﻿using AutoMapper;
using System;

namespace DBox.Communications.MappingProfiles
{
    public class MainProfile : Profile
    {
        public MainProfile()
        {

            CreateMap<Controllers.SendEmailRequest, Services.SendEmailCommand>();
            CreateMap<Controllers.SendSmsRequest, Services.SendSmsCommand>();
        }
    }
}

using Autofac;
using Consul;
using DBox.Communications.Configuration;
using DBox.Communications.Extensions;
using DBox.Communications.Services;
using DBox.Extensions;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Serilog;
using Skytecs.UniSenderApiClient;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;

namespace DBox.Communications
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<SmtpServiceSettings>(Configuration.GetSection(SmtpServiceSettings.SectionName));
            services.Configure<UnisenderServiceSettings>(Configuration.GetSection(UnisenderServiceSettings.SectionName));

            services.AddControllers();
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>().AddSwaggerGen();
            services.AddAutoMapper(GetType().Assembly);

            services.AddScoped<ISmscService, SmsGatewayService>();
            //services.AddScoped<IEmailsService, SmtpService>();
            services.AddScoped<IEmailsService, UnisenderEmailsService>();
            services.AddSingleton<IConsulClient, ConsulClient>();

            services.AddMassTransit(x =>
            {
                x.AddConsumers(typeof(Program).Assembly);

                x.SetKebabCaseEndpointNameFormatter();
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration.GetValue("RabbitHost", "localhost"), Configuration.GetValue("RabbitVirtualHost", "/"), h =>
                    {
                        h.Username(Configuration.GetValue("RabbitUsername", "guest"));
                        h.Password(Configuration.GetValue("RabbitPassword", "guest"));

                    });
                    cfg.ConfigureEndpoints(context);
                });
            });

            services.AddUniOneClient(options =>
            {
                var url = Configuration["Unisender:UniOne:BaseUrl"];
                if (!string.IsNullOrWhiteSpace(url))
                {
                    options.BaseUrl = new Uri(url);
                }
                options.ApiKey = Configuration["Unisender:UniOne:ApiKey"];
                options.Username = Configuration["Unisender:UniOne:Username"];
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSerilogRequestLogging();
            app.UseStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "swagger";
                c.SwaggerEndpoint("v1/swagger.json", "DBox Communications API V1");
            });

            app.RegisterWithConsul(lifetime);

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

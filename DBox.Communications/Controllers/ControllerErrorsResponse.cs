﻿using DBox.Communications.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBox.Communications.Controllers
{
    public abstract class ControllerErrorsResponse<T> where T : Enum
    {
        public List<ControllerError<T>> Errors { get; set; }

        public ControllerErrorsResponse()
        {
            Errors = new List<ControllerError<T>>();
        }

        public ControllerErrorsResponse(IEnumerable<T> errors)
        {
            Errors = errors?.Select(x => new ControllerError<T>
            {
                Status = x,
                Message = x.GetDescription()
            }).ToList() ?? new List<ControllerError<T>>();
        }
    }

    public class ControllerError<T> where T : Enum
    {
        public T Status { get; set; }
        public string Message { get; set; }
    }
}

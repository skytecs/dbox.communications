﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;

namespace DBox.Communications.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmsController : ControllerBase
    {
        private readonly ILogger<SmsController> _logger;
        private readonly IMapper _mapper;
        private readonly Services.ISmscService _smscService;

        public SmsController(ILogger<SmsController> logger, Services.ISmscService smscService, IMapper mapper)
        {
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _smscService = smscService ?? throw new System.ArgumentNullException(nameof(smscService));
            _mapper = mapper ?? throw new System.ArgumentNullException(nameof(mapper));
        }

        [HttpPost(Name = nameof(SendSmsAsync))]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> SendSmsAsync([FromBody] SendSmsRequest request)
        {
            var command = _mapper.Map<Services.SendSmsCommand>(request);

            await _smscService.SendMessage(command);

            if (command.HasErrors)
            {
                var errors = new List<SmsControllerError>();

                if (command.HasError(Services.SendSmsCommand.Internal))
                {
                    errors.Add(SmsControllerError.Internal);
                }
                if (command.HasError(Services.SendSmsCommand.RecepientRequired))
                {
                    errors.Add(SmsControllerError.RecepientRequired);
                }
                if (command.HasError(Services.SendSmsCommand.TextRequired))
                {
                    errors.Add(SmsControllerError.TextRequired);
                }

                return BadRequest(new SmsControllerErrorsResponse(errors));
            }

            return NoContent();
        }
    }

    #region models
    public class SendSmsRequest
    {
        public string Recepient { get; set; }
        public string Text { get; set; }
    }

    public enum SmsControllerError
    {
        [Description("Внутренняя ошибка сервера")]
        Internal = -1,
        [Description("Неизвестная ошибка")]
        Unknown = 0,
        [Description("Необходимо указать адресата")]
        RecepientRequired = 1,
        [Description("Необходимо указать текст SMS")]
        TextRequired = 2,
    }

    public class SmsControllerErrorsResponse : ControllerErrorsResponse<SmsControllerError>
    {
        public SmsControllerErrorsResponse(IEnumerable<SmsControllerError> errors) : base(errors) { }
    }
    #endregion
}

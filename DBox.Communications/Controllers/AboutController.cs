﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DBox.Communications.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AboutController : ControllerBase
    {
        [HttpGet("app", Name = nameof(AppAsync))]
        [AllowAnonymous]
        public async Task<IActionResult> AppAsync()
        {
            var response = new AppInfo
            {
                Version = $"{GetType().Assembly.GetName().Version}"
            };

            return Ok(response);
        }
    }

    #region models
    public class AppInfo
    {
        public string Version { get; set; }
    }
    #endregion
}

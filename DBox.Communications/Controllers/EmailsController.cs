﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;

namespace DBox.Communications.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailsController : ControllerBase
    {
        private readonly ILogger<EmailsController> _logger;
        private readonly IMapper _mapper;

        private readonly Services.IEmailsService _emailsService;

        public EmailsController(ILogger<EmailsController> logger, IMapper mapper, Services.IEmailsService emailsService)
        {
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new System.ArgumentNullException(nameof(mapper));
            _emailsService = emailsService ?? throw new System.ArgumentNullException(nameof(emailsService));
        }

        [HttpPost(Name = nameof(SendEmailAsync))]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType(typeof(EmailsControllerErrorsResponse), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> SendEmailAsync([FromBody] SendEmailRequest request)
        {
            var command = _mapper.Map<Services.SendEmailCommand>(request);

            await _emailsService.SendEmailAsync(command);

            if (command.HasErrors)
            {
                var errors = new List<EmailsControllerError>();

                if (command.HasError(Services.SendEmailCommand.Internal))
                {
                    errors.Add(EmailsControllerError.Internal);
                }
                if (command.HasError(Services.SendEmailCommand.BodyRequired))
                {
                    errors.Add(EmailsControllerError.BodyRequired);
                }
                if (command.HasError(Services.SendEmailCommand.AddresseeRequired))
                {
                    errors.Add(EmailsControllerError.AddresseeRequired);
                }
                if (command.HasError(Services.SendEmailCommand.SubjectRequired))
                {
                    errors.Add(EmailsControllerError.SubjectRequired);
                }

                return BadRequest(new EmailsControllerErrorsResponse(errors));
            }

            return NoContent();
        }
    }

    #region models
    public class SendEmailRequest
    {
        public string Addressee { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }

    public enum EmailsControllerError
    {
        [Description("Внутренняя ошибка сервера")]
        Internal = -1,
        [Description("Неизвестная ошибка")]
        Unknown = 0,
        [Description("Необходимо указать адресата")]
        AddresseeRequired = 1,
        [Description("Необходимо указать тему письма")]
        SubjectRequired = 2,
        [Description("Необходимо указать текст письма")]
        BodyRequired = 3,
    }

    public class EmailsControllerErrorsResponse : ControllerErrorsResponse<EmailsControllerError>
    {
        public EmailsControllerErrorsResponse(IEnumerable<EmailsControllerError> errors) : base(errors) { }
    }
    #endregion
}

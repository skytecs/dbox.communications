﻿using Microsoft.Extensions.Logging;
using System;

namespace DBox.Communications.Extensions
{
    public static class LoggerExtensions
    {
        public static ILogger<T> LogOperationStarted<T>(this ILogger<T> logger, string action, string entity, string identifier = "")
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            logger.LogInformation($"Attempt to {{Action}} {{Entity}} {{Identifier}} has {{Phase}}", action, entity, identifier, "started");

            return logger;
        }

        public static ILogger<T> LogOperationCompleted<T>(this ILogger<T> logger, string action, string entity, string identifier = "")
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            logger.LogInformation($"Attempt to {{Action}} {{Entity}} {{Identifier}} {{Phase}}", action, entity, identifier, "completed");

            return logger;
        }
        public static ILogger<T> LogOperationFailed<T>(this ILogger<T> logger, string action, string entity, string error, string identifier = "")
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            logger.LogWarning($"Attempt to {{Action}} {{Entity}} {{Identifier}} has {{Phase}}: {error}", action, entity, identifier, "failed");

            return logger;
        }

        public static ILogger<T> LogOperationFailed<T>(this ILogger<T> logger, string action, string entity, Exception e, string identifier = "")
        {
            if (logger is null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            logger.LogError(e, $"Attempt to {{Action}} {{Entity}} {{Identifier}} has {{Phase}}", action, entity, identifier, "failed");

            return logger;
        }
    }
}

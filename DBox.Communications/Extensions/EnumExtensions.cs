﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace DBox.Communications.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            if (value is null)
            {
                return null;
            }
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return attributes?.FirstOrDefault()?.Description ?? value.ToString();
        }
    }
}

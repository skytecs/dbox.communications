﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBox.Api.Contracts.Events
{
    public interface TextMessage
    {
        string[] Senders { get; set; }
        string[] Recepients { get; set; }
        string Text { get; set; }
    }
    public interface IncomingTextMessage : TextMessage { }
    public interface OutgoingTextMessage : TextMessage { }

}

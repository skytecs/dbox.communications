﻿using System.Threading.Tasks;

namespace DBox.Communications.Services
{
    public interface IEmailsService
    {
        Task SendEmailAsync(SendEmailCommand command);
    }

    #region models

    public abstract class EmailsServiceCommand : ServiceCommand { }

    public class SendEmailCommand : EmailsServiceCommand
    {
        public string Addressee { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

        public static Error AddresseeRequired = Error.Required(nameof(Addressee));
        public static Error SubjectRequired = Error.Required(nameof(Subject));
        public static Error BodyRequired = Error.Required(nameof(Body));
    }
    #endregion
}

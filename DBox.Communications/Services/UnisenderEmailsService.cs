﻿using DBox.Communications.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Skytecs.UniSenderApiClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yandex.Cloud.Iam.V1;

namespace DBox.Communications.Services
{
    public class UnisenderEmailsService : IEmailsService
    {
        private readonly IUniOneClient _uniOneClient;
        private readonly ILogger<UnisenderEmailsService> _logger;
        private readonly UnisenderServiceSettings _settings;
        public UnisenderEmailsService(IUniOneClient uniOneClient,
            ILogger<UnisenderEmailsService> logger,
            IOptionsSnapshot<UnisenderServiceSettings> settings)
        {
            _uniOneClient = uniOneClient ?? throw new System.ArgumentNullException(nameof(uniOneClient));
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            _settings = settings.Value ?? throw new System.ArgumentNullException(nameof(settings)); ;
        }

        public async Task SendEmailAsync(SendEmailCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Send", "Email", command.Addressee);

            command.CheckNotEmpty(command.Addressee, SendEmailCommand.AddresseeRequired);
            command.CheckNotEmpty(command.Subject, SendEmailCommand.SubjectRequired);
            command.CheckNotEmpty(command.Body, SendEmailCommand.BodyRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Send", "Email", string.Join(", ", command.Errors), command.Addressee);
                return;
            }

            try
            {
                var result = await _uniOneClient.Send(new UniOneSendRequest {
                    Message = new UniOneMessage
                    {
                        Subject = command.Subject,
                        Body = new Body {
                            Html = command.Body
                        },
                        Recipients = new List<Recipient>
                        {
                            new Recipient { Email = command.Addressee }
                        },
                        FromEmail = _settings.Username
                    }
                });

                _logger.LogOperationCompleted("Send", "Email", command.Addressee);
            }
            catch (Exception e)
            {
                _logger.LogOperationFailed("Send", "Email", e, command.Addressee);
                command.AddError(SendEmailCommand.Internal);
            }
        }
    }

    #region Models
    public class UnisenderServiceSettings
    {
        public const string SectionName = "Unisender:UniOne";

        public string BaseUrl { get; set; }
        public string Username { get; set; }
        public string ApiKey { get; set; }
    }
    #endregion
}

﻿using DBox.Communications.Extensions;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Threading.Tasks;

namespace DBox.Communications.Services
{
    public class SmtpService : IEmailsService
    {
        private readonly SmtpServiceSettings _settings;
        private readonly ILogger<SmtpService> _logger;

        public SmtpService(IOptionsSnapshot<SmtpServiceSettings> settings, ILogger<SmtpService> logger)
        {
            _settings = settings.Value ?? throw new ArgumentNullException(nameof(SmtpServiceSettings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task SendEmailAsync(SendEmailCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Send", "Email", command.Addressee);

            command.CheckNotEmpty(command.Addressee, SendEmailCommand.AddresseeRequired);
            command.CheckNotEmpty(command.Subject, SendEmailCommand.SubjectRequired);
            command.CheckNotEmpty(command.Body, SendEmailCommand.BodyRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Send", "Email", string.Join(", ", command.Errors), command.Addressee);
                return;
            }

            var message = new MimeMessage();

            message.From.Add(new MailboxAddress(_settings.Alias, _settings.Username));
            message.To.Add(new MailboxAddress("", command.Addressee));
            message.Subject = command.Subject;
            message.Body = new TextPart(TextFormat.Html) { Text = command.Body };

            try
            {
                using var client = new SmtpClient();
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                await client.ConnectAsync(_settings.Host, _settings.Port, true);

                await client.AuthenticateAsync(_settings.Username, _settings.Password);

                await client.SendAsync(message);
                await client.DisconnectAsync(true);

                _logger.LogOperationCompleted("Send", "Email", command.Addressee);
            }
            catch (Exception e)
            {
                _logger.LogOperationFailed("Send", "Email", e, command.Addressee);
                command.AddError(SendEmailCommand.Internal);
            }
        }
    }

    #region models
    public class SmtpServiceSettings
    {
        public const string SectionName = nameof(SmtpServiceSettings);

        public string Host { get; set; }
        public int Port { get; set; }
        public string Alias { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    #endregion
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DBox.Communications.Services
{
    public class Error
    {
        private readonly string _message;

        public Error(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException($"\"{nameof(message)}\" не может быть пустым или содержать только пробел.", nameof(message));
            }

            _message = message;
        }

        public string Message => _message;

        public static Error Required(string field) => new Error($"{field} is required");

        public override string ToString() => Message;

    }

    public abstract class ServiceCommand
    {
        private readonly HashSet<Error> _errors;

        public ServiceCommand()
        {
            _errors = new HashSet<Error>();
        }

        public void AddError(Error error)
        {
            if (!_errors.Contains(error))
            {
                _errors.Add(error);
            }
        }

        public bool HasErrors => _errors.Any();

        public bool HasError(Error error) => _errors.Contains(error);

        public List<Error> Errors => _errors.ToList();

        public static Error Internal = new Error("Internal server error");
    }

    public static class ServiceCommandExtensions
    {
        public static void CheckNotEmpty(this ServiceCommand command, string value, Error error)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            if (error is null)
            {
                throw new ArgumentNullException(nameof(error));
            }

            if (string.IsNullOrEmpty(value))
            {
                command.AddError(error);
            }
        }
    }
}

﻿using System.Threading.Tasks;

namespace DBox.Communications.Services
{
    public interface ISmscService
    {
        Task SendMessage(SendSmsCommand command);
    }
}

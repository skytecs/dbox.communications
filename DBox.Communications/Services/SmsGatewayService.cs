﻿using DBox.Api.Contracts.Events;
using DBox.Communications.Extensions;
using MassTransit;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace DBox.Communications.Services
{
    public class SmsGatewayService : ISmscService
    {
        private readonly ILogger<SmsGatewayService> _logger;
        private readonly IPublishEndpoint _publishEndpoint;

        public SmsGatewayService(ILogger<SmsGatewayService> logger, IPublishEndpoint publishEndpoint)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _publishEndpoint = publishEndpoint ?? throw new ArgumentNullException(nameof(publishEndpoint));
        }

        public async Task SendMessage(SendSmsCommand command)
        {
            if (command is null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            _logger.LogOperationStarted("Send", "Sms", command.Recepient);
            _logger.LogOperationStarted("Send", "Sms content", command.Text);

            command.CheckNotEmpty(command.Recepient, SendSmsCommand.RecepientRequired);
            command.CheckNotEmpty(command.Text, SendSmsCommand.TextRequired);

            if (command.HasErrors)
            {
                _logger.LogOperationFailed("Send", "Sms", string.Join(", ", command.Errors), command.Recepient);
                return;
            }

            try
            {
                await _publishEndpoint.Publish<OutgoingTextMessage>(new
                {
                    Text = command.Text,
                    Recepients = command.Recepient.Split(",", StringSplitOptions.RemoveEmptyEntries),
                    Senders = string.IsNullOrWhiteSpace(command.Sender) ? new string[0] : new string[] { command.Sender }
                });
            }
            catch (Exception e)
            {
                _logger.LogOperationFailed("Send", "Sms", e, command.Recepient);
                command.AddError(SendSmsCommand.Internal);
                return;
            }
        }
    }

    #region Models

    public class SendSmsCommand : ServiceCommand
    {
        public string Recepient { get; set; }
        public string Text { get; set; }
        public string Sender { get; set; }

        public static Error RecepientRequired = Error.Required(nameof(Recepient));
        public static Error TextRequired = Error.Required(nameof(Text));
    }

    #endregion
}
